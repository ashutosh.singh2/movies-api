("use strict")
const fs = require(`/home/ashutosh/projects/movies-Api/src/scripts/movies.json`)
const conns = require(`./src/utils/sqlconnection`)
conn = conns.connection();


async function execute() {
  conn.connect(function (error) {
    if (error) throw error;
    console.log("connected");

  });
  // const tableName = `Director`;
  // const tableName='movies'
  // await createdirectorTable();
  // await insertDirectorTable();
  //  await dropTables(tableName);
  // await createTableMovies();
  // insertMoviesData();
  // await GetAllDirector();
  // directorOnId(36);
  // addDirector('ashutosh');
  // UpdateDirector(36,"punit")
  // deleteDirector(36)
  // getAllMovies()
  // moviesId(4);
}
execute();

function dropTables(tableName) {
  return new Promise((resolve, reject) => {
    var sql = `drop table ${tableName}`;
    conn.query(sql, function (err, result) {
      if (err) throw err;
      resolve(console.log("Table deleted"));
    });
  });
}

function createTableMovies() {
  return new Promise((resolve, reject) => {
    var sql = `create table  movies(id int auto_increment primary key ,Rank int(10),Title varchar(500),Description varchar(1000),Runtime int(10),Genre varchar(500),Rating int(10),Metascore varchar(10),votes int(15),Gross_Earning_in_Mil varchar(10),director_id int ,Actor varchar(500),Year int(10),FOREIGN KEY (director_id) REFERENCES Directors(directorId)on update cascade on delete cascade)`;
    conn.query(sql, function (err, result) {
      if (err) throw err;
      resolve(console.log("Table created"));
    });
  })
}

function createdirectorTable() {
  return new Promise((resolve, reject) => {
    let sql = 'CREATE TABLE Directors(directorId int auto_increment primary key,name varchar(500) unique)';
    conn.query(sql, (err, result) => {
      if (err) throw err;
      resolve(console.log("director table created"))
    })
  })

}

function insertMoviesData() {
  fs.forEach(row => {

    let directorId = `select directorId from Directors where name="${row[`Director`]}"`;
    conn.query(directorId, function (err, output) {
      let sql = `insert into movies(Rank,Title,Description,Runtime,Genre,Rating,Metascore,Votes,Gross_Earning_in_Mil,director_id,Actor,Year) values(${row[`Rank`]},"${row[`Title`]}","${row[`Description`]}",${row[`Runtime`]},"${row[`Genre`]}",${row[`Rating`]},"${row[`Metascore`]}",${row[`Votes`]},"${row[`Gross_Earning_in_Mil`]}","${output[0][`directorId`]}","${row[`Actor`]}",${row[`Year`]})`;
      conn.query(sql, function (err, result) {
        if (err) throw err;
      });
    });
  });
  console.log(`inserted`)
}

function insertDirectorTable() {

  let array=[] ;
  return new Promise((resolve, reject) => {
    fs.forEach(element => {
      if(array.includes(element['Director'])){

      }
      else{
      array.push(element['Director'])
      let sql = `insert into Directors(name) values("${element['Director']}")`
      conn.query(sql, (err, result) => {
        if (err) console.log(err);
      });
      }
    })
    resolve(console.log("director Table Loaded"))
  });
}

function GetAllDirector() {
  return new Promise((resolve, reject) => {
    let sql = `select * from Directors`
    conn.query(sql, (err, directors) => {
      if (err) throw err;
      console.log(directors)
      resolve(directors)
    })
  })
}
module.exports.GetAllDirector = GetAllDirector;

function directorOnId(directorId) {
  return new Promise((resolve, reject) => {
    let sql = `select name from Directors where directorId=${directorId}`
    conn.query(sql, (err, director) => {
      if (err) console.log(err);
      console.log(director)
      resolve(director)
    })
  })
}
module.exports.directorOnId = directorOnId;

function addDirector(directorName) {
  let sql = `insert into Directors(name) values("${directorName}")`
  conn.query(sql, (err, result) => {
    if (err) throw err;

  })

}
module.exports.addDirector = addDirector;

function UpdateDirector(id, name) {
  let sql = `update Directors set name="${name}" where directorId=${id}`
  conn.query(sql, (err, update) => {
    if (err) throw err;
    console.log("updated")
  })
}
module.exports.UpdateDirector = UpdateDirector;

function deleteDirector(id) {
  let sql = `delete from Directors where directorId=${id}`
  conn.query(sql, (err, del) => {
    if (err) throw err;
    console.log("deleted")
  })
}
module.exports.deleteDirector = deleteDirector;
// movies  row sql query  ---------------
function getAllMovies() {
  return new Promise((resolve, reject) => {
    let sql = `select * from movies`
    conn.query(sql, (err, movies) => {
      if (err) throw err;
      console.log(movies)
      resolve(movies)
    })
  })
}
module.exports.getAllMovies = getAllMovies;

function moviesId(id) {
  return new Promise((resolve, reject) => {
    let sql = `select * from movies where director_id=${id}`
    conn.query(sql, (err, title) => {
      if (err) throw err;
      resolve(title);
    })
  })
}
module.exports.moviesId = moviesId;
let body = {
  "Rank": 70,

  "Title": "The Shawshank Redemption",

  "Description": "Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.",

  "Runtime": 142,

  "Genre": "Crime",

  "Rating": 9.3,

  "Metascore": 80,

  "Votes": 1934970,

  "Gross_Earning_in_Mil": 28.34,

  "Director": "ashutosh",

  "Actor": "Tim Robbins",

  "Year": 1994

}


function addMovie(body) {
  let directorId = `select directorId from Director where name="${body[`Director`]}"`;
  conn.query(directorId, function (err, output) {
    if (err) {
      console.log(err)
      let addDirector = `insert into Director(name) values("${body[`Director`]}");`
      conn.query(addDirector, function (err, id) {
        console.log(err)
        let directorId = `select directoId from Director where name="${body[`Director`]}"`;
        conn.query(directorId, function (err, output) {
          console.log(output)
          let sql = `insert into movies(Rank,Title,Description,Runtime,Genre,Rating,Metascore,Votes,Gross_Earning_in_Mil,director_id,Actor,Year) values(${body[`Rank`]},"${body[`Title`]}","${body[`Description`]}",${body[`Runtime`]},"${body[`Genre`]}",${body[`Rating`]},"${body[`Metascore`]}",${body[`Votes`]},"${body[`Gross_Earning_in_Mil`]}","${output[0][`directorId`]}","${body[`Actor`]}",${body[`Year`]})`;
          conn.query(sql, function (err, result) {
            if (err) throw err;
          });
        })
      })
    } else {
      console.log(output)
      let sql = `insert into movies(Rank,Title,Description,Runtime,Genre,Rating,Metascore,Votes,Gross_Earning_in_Mil,director_id,Actor,Year) values(${body[`Rank`]},"${body[`Title`]}","${body[`Description`]}",${body[`Runtime`]},"${body[`Genre`]}",${body[`Rating`]},"${body[`Metascore`]}",${body[`Votes`]},"${body[`Gross_Earning_in_Mil`]}","${output[0][`directorId`]}","${body[`Actor`]}",${body[`Year`]})`;
      conn.query(sql, function (err, result) {
        if (err) throw err;
      });
    }
  });
}
// addMovie(x)
  // function updateMovies(body) {
  //   let sql = `update movies `
  // }
  // function deletemovie(id){
  //   let sql = `delete from movies where id="${id}"`;
  //   conn.query(sql,(err,deleting)=>{
  //     if(err) throw err;
  //   })
  // }
