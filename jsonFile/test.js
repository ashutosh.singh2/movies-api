("use strict")
fetch('./movies.json')
const mysql=require('mysql');
const connection=mysql.createConnection({
    host :"localhost",
    user:"root",
    password:"ashu05@ASHU",
    database: "moviesApi"
});
connection.connect(function(error){
    if(error) throw error;
    console.log("connected");
    // tableName='customers';
    // dropTables(tableName)
    createTableMovies()
    
});
function dropTables(tableName){
var sql = `drop table ${tableName}`;
connection.query(sql, function (err, result) {
  if (err) throw err;
  console.log("Table deleted");
});
}
function createTableMovies(){
     var sql = `CREATE TABLE movies(id int  NOT NULL AUTO_INCREMENT ,Rank int(10),Title varchar(100),Description varchar(1000),Runtime int(10),Genre varchar(50),Rating int(10),Metascore int(10),votes int(15),Gross_Earning_in_Mil int(10),Director varchar(50),Actor varchar(50),Year int(10),primary key(id))`;
    connection.query(sql, function (err, result) {
      if (err) throw err;
      console.log("Table created");
    });
}