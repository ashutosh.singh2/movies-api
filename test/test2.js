("use strict")
const director = require('../test')
const express = require('express');
const path = require('path')
const app = express();
const bodyParser = require('body-parser')
app.use(bodyParser())

async function executeDirector() {
    // const directors = await director.GetAllDirector();
    // const directorOnId =

    app.get('/api/directors', (req, respond) => {
        director.GetAllDirector().then(x => {
            respond.json(x)
        })

    })

    // question 2
    app.get('/api/directors/id/:id', (req, res) => {
        director.directorOnId(parseInt(req.params.id)).then(x => {
            res.json(x);
        })
    });
    // question 3 
    app.post('/api/directors', (req, res) => {
        // console.log(req.body['Director']);
        director.addDirector(req.body['Director'])
        res.json("completed")
    });

    // question 4
    app.delete('/api/directors/id/:id', (req, res) => {
        director.deleteDirector(req.params.id)
        res.json("completed")
    });
    // updTate director
    app.put('/api/directors/id/:id/name/:name', (req, res) => {

        director.UpdateDirector(req.params.id, req.body['Director'])
        res.json("completed")
    });
    // get all movies

}
executeDirector();
async function executeMovies() {
    //get all movies -----------
    app.get('/api/movies', (req, respond) => {
        director.getAllMovies().then(x => {
            respond.json(x)
        })
    })


    // get movies by id
    app.get('/api/movies/id/:id', (req, res) => {
        director.moviesId(parseInt(req.params.id)).then(x => {
            res.json(x);
        })
    });
    //add movies
    app.post('/api/moives', (req, response) => {
        console.log(req.body)
        // director.addMovie(req.body)
        res.json("completed")
    })
    //update movies 
    app.put('/api/movies/id/:id', (req, res) => {

    })

    // delete movies 
    app.delete('/api/movies/id/:id', (req, res) => {

    })
}
executeMovies();
const logger = (request, response, next) => {
    console.log(`${request.protocol}://${request.get('host')}${request.originalUrl}`);
    
}
//init moddleware
app.use(logger);
// get all members 


// get single member 

// app.get('/api/directors/id/:id',(req,res)=>{

//     res.send(req.params.id)});

// set a static folder
// app.use(express.static(path.join(__dirname, 'movies-Api')));

const port = process.env.port || 3000;
app.listen(port, () => console.log(port));