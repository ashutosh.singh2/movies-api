const members = require('/home/ashutosh/projects/movies-Api/src/scripts/movies.json')
const express = require('express');
const path = require('path')
const app = express();
const logger = (request,response,next)=>{
    console.log(`${request.protocol}://${request.get('host')}${request.originalUrl}`);
    next();
}
//init moddleware
app.use(logger);
    // get all members 
    app.get('/api/members', (req, respond) => {
        respond.json(members)
    })

// get single member 
       
app.get('/api/members/:Rank',(request,response)=>{
response.send(request.params.id);
});

// set a static folder
// app.use(express.static(path.join(__dirname, 'movies-Api')));

const port = process.env.port || 8080;
app.listen(port, () => console.log(port));