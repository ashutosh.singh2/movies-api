("use strict");
const jasonFile = require(`../scripts/movies.json`);
const conns = require(`../utils/sqlconnection`);
conn = conns.connection();

async function execute() {
  conn.connect(function(error) {
    if (error) throw error;
    console.log("connected");
  });
  await dropTables("movies");
  await dropTables("Directors");
  await createDirectorTable();
  let directorName = await filterDiector();
  await insertDirectorTable(directorName);
  await createTableMovies();
  insertMoviesData();
}
execute();

function dropTables(tableName) {
  return new Promise((resolve, reject) => {
    var sql = `drop table ${tableName}`;
    conn.query(sql, function(err, result) {
      if (err) throw err;
      resolve(console.log("Table deleted"));
    });
  });
}

function createDirectorTable() {
  return new Promise((resolve, reject) => {
    let sql =
      "CREATE TABLE Directors(directorId int auto_increment primary key,name varchar(500) unique)";
    conn.query(sql, (err, result) => {
      if (err) throw err;
      resolve(console.log("director table created"));
    });
  });
}

function insertDirectorTable(directorName) {
  return new Promise((resolve, reject) => {
    directorName.forEach(element => {
      let sql = `insert into Directors(name) values("${element}")`;
      conn.query(sql, (err, result) => {
        if (err) console.log(err);
      });
    });
    resolve(console.log("director Table Loaded"));
  });
}

function createTableMovies() {
  return new Promise((resolve, reject) => {
    var sql = `create table  movies(id int auto_increment primary key ,Rank int(10),Title varchar(500),Description varchar(1000),Runtime int(10),Genre varchar(500),Rating int(10),Metascore varchar(10),votes int(15),Gross_Earning_in_Mil varchar(10),director_id int ,Actor varchar(500),Year int(10),FOREIGN KEY (director_id) REFERENCES Directors(directorId)on update cascade on delete cascade)`;
    conn.query(sql, function(err, result) {
      if (err) throw err;
      resolve(console.log("Table created"));
    });
  });
}

function insertMoviesData() {
  jasonFile.forEach(row => {
    let directorId = `select directorId from Directors where name="${
      row[`Director`]
    }"`;
    conn.query(directorId, function(err, output) {
      let sql = `insert into movies(Rank,Title,Description,Runtime,Genre,Rating,Metascore,Votes,Gross_Earning_in_Mil,director_id,Actor,Year) values(${
        row[`Rank`]
      },"${row[`Title`]}","${row[`Description`]}",${row[`Runtime`]},"${
        row[`Genre`]
      }",${row[`Rating`]},"${row[`Metascore`]}",${row[`Votes`]},"${
        row[`Gross_Earning_in_Mil`]
      }","${output[0][`directorId`]}","${row[`Actor`]}",${row[`Year`]})`;
      conn.query(sql, function(err, result) {
        if (err) throw err;
      });
    });
  });
  console.log(`inserted`);
}

function filterDiector() {
  return new Promise((resolve, reject) => {
    let array = [];
    jasonFile.forEach(element => {
      if (array.includes(element["Director"])) {
      } else {
        array.push(element["Director"]);
      }
    });
    resolve(array);
  });
}
