let mysql = require("mysql");
const conns = require(`../utils/sqlconnection`);
conn = conns.connection();

function getAllDirector() {
  return new Promise((resolve, reject) => {
    let sql = `select * from ?? order by ??`;
    let inserts = ["Directors", "directorId"];
    sql = mysql.format(sql, inserts);
    conn.query(sql, (err, directors) => {
      if (err) throw err;
      resolve(directors);
    });
  });
}
// getAllDirector();
module.exports.getAllDirector = getAllDirector;

function directorOnId(directorId) {
  return new Promise((resolve, reject) => {
    let sql = `select * from ?? where ?? =?`;
    let inserts = ["Directors", "directorId", directorId];
    sql = mysql.format(sql, inserts);
    conn.query(sql, (err, director) => {
      if (err) console.log(err);
      resolve(director);
    });
  });
}
// directorOnId(5)
module.exports.directorOnId = directorOnId;

function addDirector(directorName) {
  return new Promise((resolve, reject) => {
    let sql = `insert into ?? (??)  values(?)`;
    let inserts = ["Directors", "name", directorName];
    sql = mysql.format(sql, inserts);
    conn.query(sql, (err, result) => {
      if (err) throw err;
      let sql = `select * from ?? where ?? =?`;
      let inserts = ["Directors", "name", directorName];
      sql = mysql.format(sql, inserts);
      conn.query(sql, (err, result) => {
        resolve(result);
      });
    });
  });
}
// addDirector('ashu')
module.exports.addDirector = addDirector;

function updateDirector(id, name) {
  // let sql = `update Directors set name="${name}" where directorId=${id}`
  return new Promise((resolve, reject) => {
    let sql = "update ?? set name = ? where ?? = ?";
    let inserts = ["Directors", name, "directorId", id];
    sql = mysql.format(sql, inserts);
    conn.query(sql, (err, update) => {
      if (err) throw err;
      resolve(directorOnId(id));
    });
  });
}
// updateDirector(36,"puneet")
module.exports.updateDirector = updateDirector;

function deleteDirector(id) {
  return new Promise((resolve, reject) => {
    let sql = `select * from ?? where ?? =?`;
    let inserts = ["Directors", "directorId", id];
    sql = mysql.format(sql, inserts);
    conn.query(sql, (err, output) => {
      let sql = "delete from ?? where ?? = ?";
      let inserts = ["Directors", "directorId", id];
      sql = mysql.format(sql, inserts);
      conn.query(sql, (err, del) => {
        if (err) throw err;

        resolve(output);
      });
    });

    // let sql = `delete from Directors where directorId=${id}`
  });
}
// deleteDirector(36)
module.exports.deleteDirector = deleteDirector;

function getDirectorNameMovie(directorId) {
  return new Promise((resolve, reject) => {
    // let sql = 'select movies.Title, Directors.name from movies join Directors on movies.director_id = Directors.directorId where Directors.directorId=2;'
    let sql = `select ?? , ?? from ?? join ?? on ?? = ?? where ?? =?`;
    let inserts = [
      `movies.Title`,
      `Directors.name`,
      `movies`,
      `Directors`,
      `movies.director_id`,
      `Directors.directorId`,
      `Directors.directorId`,
      directorId
    ];
    sql = mysql.format(sql, inserts);
    conn.query(sql, (err, output) => {
      if (err) throw err;
      resolve(output);
      // console.log(output)
    });
  });
}
// getDirectorNameMovie(2)
module.exports.getDirectorNameMovie = getDirectorNameMovie;
