const joi = require("joi")

const idSchema = joi.object().keys({
    id:joi.number().min(1).required()
})
module.exports.idSchema = idSchema;

const directorSchema = joi.object().keys({
    Director: joi.string().required()
});


module.exports.directorSchema = directorSchema;


const moviesPostSchema = joi.object().keys({
    Rank: joi.number().required(),
    Title: joi.string().required(),
    Description: joi.string().trim().required(),
    Runtime: joi.number().required(),
    Director: joi.string().trim().required(),
    Genre: joi.string().trim().required(),
    Rating: joi.number().required(),
    Metascore: joi.string().required(),
    Votes: joi.number().required(),
    Gross_Earning_in_Mil: joi.string().required(),
    Actor: joi.string().required(),
    Year: joi.number().required()
})
module.exports.moviesPostSchema = moviesPostSchema;
const movieUpdateSchema = joi.object().keys({
    Rank: joi.number().integer(),
    Title: joi.string(),
    Description: joi.string(),
    Runtime: joi.number(),
    Director: joi.string(),
    Genre: joi.string(),
    Rating: joi.number(),
    Metascore: joi.string(),
    Votes: joi.number(),
    Gross_Earning_in_Mil: joi.string(),
    Actor: joi.string(),
    Year: joi.number()
})

module.exports.movieUpdateSchema = movieUpdateSchema;