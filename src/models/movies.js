const mysql = require('mysql')
const conns = require(`../utils/sqlconnection`)
conn = conns.connection();

function getAllMovies() {
  return new Promise((resolve, reject) => {
    // let sql = `select * from movies`
    let sql = 'select * from ??'
    let inserts = ['movies']
    sql = mysql.format(sql, inserts)
    conn.query(sql, (err, movies) => {
      if (err) throw err;
      // console.log(movies)
      resolve(movies)
      reject("SQL QUERY IS WRONG")
    })
  })
}
// getAllMovies();
module.exports.getAllMovies = getAllMovies;

function moviesId(id) {
  return new Promise((resolve, reject) => {
    // let sql = `select * from movies where id=${id}`
    let sql = 'select * from ??where ?? = ? '
    let inserts = ['movies', 'id', id]
    sql = mysql.format(sql, inserts)
    conn.query(sql, (err, title) => {
      if (err) throw err;
      //  console.log
      resolve(title);
    })
  })
}
// moviesId(4);
module.exports.moviesId = moviesId;

let x = {
  "Rank": 70,

  "Title": "tERMINATOR",

  "Description": "Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.",

  "Runtime": 142,

  "Genre": "Crime",

  "Rating": 9.3,

  "Metascore": 80,

  "Votes": 1934970,

  "Gross_Earning_in_Mil": 28.34,

  "Director": "ashutosh",

  "Actor": "Tim Robbins",

  "Year": 1994

}


function addMovie(body) {
  console.log(body,"body")
  // let directorId = `select directorId from Directors where name = ${body['Director']}`;
  return new Promise((resolve, reject) => {
    let sql = 'select ?? from ?? where ?? = ?'
    let inserts = ['directorId', 'Directors', 'name', body['Director']]
    sql = mysql.format(sql, inserts)
    conn.query(sql, function (err, output) {
      if (!output[0]) {
        let addDirector = 'insert into ?? (??) values (?)'
        let inserts = ['Directors', 'name', body['Director']]
        addDirector = mysql.format(addDirector, inserts)
        conn.query(addDirector, function (err, id) {

          let directorId = 'select ?? from ?? where ?? = ?'
          let inserts = ['directorId', 'Directors', 'name', body['Director']]
          directorId = mysql.format(directorId, inserts)
          conn.query(directorId, function (err, output) {
            // let sql = `insert into movies(Rank,Title,Description,Runtime,Genre,Rating,Metascore,Votes,Gross_Earning_in_Mil,director_id,Actor,Year) values(${body[`Rank`]},"${body[`Title`]}","${body[`Description`]}",${body[`Runtime`]},"${body[`Genre`]}",${body[`Rating`]},"${body[`Metascore`]}",${body[`Votes`]},"${body[`Gross_Earning_in_Mil`]}","${output[0][`directorId`]}","${body[`Actor`]}",${body[`Year`]})`;
            let sql = 'insert into ?? (??,??,??,??,??,??,??,??,??,??,??,??) values (?,?,?,?,?,?,?,?,?,?,?,?)'
            let inserts = ['movies', 'Rank', 'Title', 'Description', 'Runtime', 'Genre', 'Rating', 'Metascore', 'Votes', 'Gross_Earning_in_Mil', 'director_id', 'Actor', 'Year', body[`Rank`], body[`Title`], body[`Description`], body[`Runtime`], body[`Genre`], body[`Rating`], body[`Metascore`], body[`Votes`], body[`Gross_Earning_in_Mil`], output[0][`directorId`], body[`Actor`], body['Year']]
            sql = mysql.format(sql, inserts)
            conn.query(sql, function (err, result) {
              if (err) throw err;
            });
          })
        })
      } else {
        console.log(output)
        // let sql = `insert into movies(Rank,Title,Description,Runtime,Genre,Rating,Metascore,Votes,Gross_Earning_in_Mil,director_id,Actor,Year) values(${body[`Rank`]},"${body[`Title`]}","${body[`Description`]}",${body[`Runtime`]},"${body[`Genre`]}",${body[`Rating`]},"${body[`Metascore`]}",${body[`Votes`]},"${body[`Gross_Earning_in_Mil`]}","${output[0][`directorId`]}","${body[`Actor`]}",${body[`Year`]})`;
        let sql = 'insert into ?? (??,??,??,??,??,??,??,??,??,??,??,??) values (?,?,?,?,?,?,?,?,?,?,?,?)'
        let inserts = ['movies', 'Rank', 'Title', 'Description', 'Runtime', 'Genre', 'Rating', 'Metascore', 'Votes', 'Gross_Earning_in_Mil', 'director_id', 'Actor', 'Year', body[`Rank`], body[`Title`], body[`Description`], body[`Runtime`], body[`Genre`], body[`Rating`], body[`Metascore`], body[`Votes`], body[`Gross_Earning_in_Mil`], output[0][`directorId`], body[`Actor`], body['Year']]
        sql = mysql.format(sql, inserts)
        conn.query(sql, function (err, result) {
          if (err) throw err;
          console.log(result)
        });
      }
    });
    resolve(moviesId(id))
  })
}
// addMovie(x)
module.exports.addMovie = addMovie;

function updateMovies(id, body) {
  return new Promise((resolve, reject) => {
    let column = " ";
    console.log(body, "body")
    for (key in body) {
      if (key == 'Gross_Earning_in_Mil' || key == 'Metascore') {
        column += `${key}="${body[key]}" ,`;
      } else if (typeof (body[key]) == 'number') {
        column += `${key}=${body[key]} ,`;
      } else
        column += `${key}="${body[key]}" ,`;
    }
    column = column.substring(0, column.lastIndexOf(" "));
    console.log(column)
    let sql = `update movies SET ${column} WHERE id=${id}`

    console.log(sql);
    conn.query(sql, (err, update) => {
      if (err) throw err;
      else
        resolve(moviesId(id))
    })
  })
}
// updateMovies(52,x)
module.exports.updateMovies = updateMovies

function deletemovie(id) {
  return new Promise((resolve, reject) => {
    let sql = 'delete from ?? where ?? = ?'
    let inserts = ['movies', 'id', id]
    sql = mysql.format(sql, inserts)
    conn.query(sql, (err, deleting) => {
      if (err) throw err;
      resolve(deleting)
    })
  })
}
// deletemovie(51)
module.exports.deletemovie = deletemovie;