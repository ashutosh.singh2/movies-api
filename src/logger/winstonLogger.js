const winston = require("winston");

const logger = winston.createLogger({
  transports: [
    new winston.transports.File({
      filename: "./error.log",
      level: "error"
    })
  ]
});
logger.error("working", new Error("error"));
module.exports = logger;
