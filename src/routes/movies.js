const fs = require("fs");
const express = require("express");
const bodyParser = require("body-parser");
const joi = require("joi");
const middleWare = require("../middleware/errorhandler");
const validate = require("../models/validation");
const winstonLogger = require("../logger/winstonLogger");
const logger = require("morgan");
const sqlQueries = require("../models/movies");
const moviesRouter = express.Router();
moviesRouter.use(bodyParser.json());
moviesRouter.use(
  bodyParser.urlencoded({
    extended: false
  })
);

// var accessLogStream = fs.createWriteStream("../models/movies.log", {
//   flags: "a"
// });
// moviesRouter.use(
//   logger("combined", {
//     stream: accessLogStream
//   })
// );

moviesRouter.get("/", (req, res, next) => {
  sqlQueries
    .getAllMovies()
    .then(x => {
      res.json(x);
    })
    .catch(err => next(err));
});

moviesRouter.get("/:id", (req, res, next) => {
  const idValidation = joi.validate(req.params, validate.idSchema);
  if (!idValidation.error) {
    sqlQueries
      .moviesId(parseInt(req.params.id))
      .then(data => {
        if (data.length === 0)
          res
            .status(404)
            .send(`Director with ID ${req.params.id} doesnt exist`);
        else res.json(data);
      })
      .catch(err => next(err));
  } else {
    winstonLogger.error(idValidation.error);
    res.status(400).send(idValidation.error.details[0].message);
  }
});

moviesRouter.post("/", (req, res, next) => {
  console.log(req.body);
  let validation = joi.validate(req.body, validate.moviesPostSchema);
  if (validation.error) {
    winstonLogger.error(validation.error);
    res.status(400).send(validation.error.details[0].message);
  } else {
    sqlQueries
      .addMovie(req.body)
      .then(data => res.json(data))
      .catch(err => next(err));
  }
});

moviesRouter.delete("/:id", (req, res, next) => {
  const idValidation = joi.validate(req.params, validate.idSchema);
  if (!idValidation.error) {
    sqlQueries
      .deletemovie(req.params.id)
      .then(data => {
        if (data.length === 0) {
          res.status(404).send("invalid id");
        } else
          sqlQueries
            .deletemovie(req.params.id)
            .then(data => res.json(data))
            .catch(err => next(err));
      })
      .catch(err => res.json(err));
  } else {
    winstonLogger.error(idValidation.error);
    res.status(400).send(idValidation.error.details[0].message);
  }
});

moviesRouter.put("/:id", (req, res, next) => {
  let validation = joi.validate(req.body, validate.movieUpdateSchema);
  if (validation.error) {
    winstonLogger.error(validation.error);
    res.status(400).send(validation.error.details[0].message);
  } else {
    sqlQueries
      .updateMovies(req.params.id, req.body)
      .then(output => {
        if (data.length === 0) {
          res.status(404).send("invalid id");
        } else {
          res.json(output);
        }
      })
      .catch(err => next(err));
  }
});

moviesRouter.use(middleWare);
module.exports = moviesRouter;
