const fs = require("fs");
const express = require("express");
const bodyParser = require("body-parser");
const validate = require("../models/validation");
const winstonLogger = require("../logger/winstonLogger");
const logger = require("morgan");
const joi = require("joi");
const middleWare = require("../middleware/errorhandler");
const sqlQueries = require("../models/directors");
// const s = require("../models/direcctors.log");
// 2 line change
// const winston = require('winston')
// const winston = require('../logger/winstonLogger')
const directorRouter = express.Router();
directorRouter.use(bodyParser.json());
directorRouter.use(
  bodyParser.urlencoded({
    extended: false
  })
);
//10 line change for error log
// directorRouter.use(router); // notice how the router goes first.
// winston.createLogger({
//   transports: [
//     new winston.transports.File({
//         filename:'/home/ashutosh/projects/movies-Api/src/models/direcctors.log',
//         level :error
//     })
//   ],
// });

// var accessLogStream = fs.createWriteStream("../models/directors.log", {
//   flags: "a"
// });
// directorRouter.use(winston)
// directorRouter.use(
//   logger("combined", {
//     stream: accessLogStream
//   })
// );

directorRouter.get("/", (req, response, next) => {
  sqlQueries
    .getAllDirector()
    .then(director => {
      response.json(director);
    })
    .catch(err => {
      // winston.error(err);
      console.log("qu  incorect");
      console.log(err);
      next(err);
    });
});

directorRouter.get("/:id", (req, res, next) => {
  const idValidation = joi.validate(req.params, validate.idSchema);
  if (!idValidation.error) {
    sqlQueries
      .directorOnId(parseInt(req.params.id))
      .then(director => {
        if (director.length === 0) {
          res.status(404).send(`Director is not exist on ${req.params.id} id`);
        } else res.json(director);
      })
      .catch(err => next(err));
  } else {
    winstonLogger.error(idValidation.error);
    res.status(400).send(idValidation.error.details[0].message);
  }
});

directorRouter.post("/", (req, res, next) => {
  const validation = joi.validate(req.body, validate.directorSchema);
  if (validation.error) {
    winstonLogger.error(Validation.error);
    res.status(400).send(validationResult.error.details[0].message);
  } else {
    sqlQueries
      .addDirector(req.body["Director"])
      .then(director => {
        res.json(director);
        res.json(validation.value);
        //yaha jo operation hota hai wo likhte hai
      })
      .catch(err => next(err));
  }
});

directorRouter.delete("/:id", (req, res, next) => {
  const idValidation = joi.validate(req.params, validate.idSchema);
  if (idValidation.error) {
    winstonLogger.error(idValidation.error);
    res.status(400).send(validationResult.error.details[0].message);
  } else {
    sqlQueries
      .deleteDirector(req.params.id)
      .then(output => {
        if (output.length === 0) {
          res.status(404).send("id is invalid ");
        } else director.deleteDirectorById(req.params.id);
      })
      .catch(err => next(err));
  }
});

directorRouter.put("/:id", (req, res, next) => {
  let idValidation = joi.validate(req.param.id, validate.idSchema);
  if (!idValidation.error) {
    sqlQueries.directorOnId(req.params.id).then(data => {
      if (data.length == 0) {
        res.status(404).send("invalid id");
      }
    });

    let validation = joi.validate(req.body, validate.directorSchema);
    if (validation.error) {
      winstonLogger.error(validation.error);
      res.status(400).send(validationResult.error.details[0].message);
    } else {
      sqlQueries
        .updateDirector(req.params.id, req.body["Director"])
        .then(data => res.json(data))
        .catch(err => next(err));
    }
  } else res.status(400).send(validationResult.error.details[0].message);
});

directorRouter.get("/name/:id", (req, res, next) => {
  let idValidation = joi.validate(req.param.id, validate.idSchema);
  if (!idValidation.error) {
    sqlQueries.getDirectorNameMovie(req.params.id).then(data => {
      if (data.length == 0) {
        res.status(404).send("invalid id");
      } else {
        res.json(data);
      }
    });
  }
});

directorRouter.use(middleWare);

module.exports = directorRouter;
