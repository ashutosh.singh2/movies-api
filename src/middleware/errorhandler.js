const winston = require("../logger/winstonLogger");

const errorHandler = (err, req, res, next) => {
  winston.error(err);
  
  res.status(500).send(err.sqlMessage);
  
};

module.exports = errorHandler;
