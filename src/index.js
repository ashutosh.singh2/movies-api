("use strict");
const express = require("express");
const app = express();
const cors = require("cors");
const path = require("path");
app.use(cors());
const bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);
app.use(express.static(path.join(__dirname, "build")));

const directorRouter = require("./routes/directors");
const moviesRouter = require("./routes/movies");
app.use("/api/directors", directorRouter);
app.use("/api/movies", moviesRouter);

app.get("/", (req, res) => {
  // res.sendFile('../build/index.html')
  res.sendFile(__dirname + "/build/index.html");
});

//

// app.use((req, res, next) => {
//   const error = new Error("Not found");
//   error.status(404);
//   next(error);
// });
// app.use((error, req, res, next) => {
//   res.status(error.status || 8080);
//   res.json({
//     error: {
//       message: error.message
//     }
//   });
// });

// const port = process.env.port || 8080;
// app.listen(port, () => console.log(port));
app.listen(8080, () => console.log("listening on the port 8080.."));
